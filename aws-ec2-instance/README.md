## AWS Create EC2 Instance

### Install AWS CLI if not yet available
``` bash
brew install awscli

aws configure --profile aws-ec2-instance
```

### If using existing AWS profile, set this to path
``` bash
export AWS_PROFILE=aws-ec2-instance
```

### Go to this `/aws-ec2-instance` directory
``` bash
aws cloudformation create-stack --stack-name aws-ec2-instance --template-body file://main.yml --parameters file://params.json --profile aws-ec2-instance

aws cloudformation delete-stack --stack-name aws-ec2-instance --profile aws-ec2-instance
```